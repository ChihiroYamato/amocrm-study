<?php

use Phpmig\Migration\Migration;
use Illuminate\Database\Schema\Blueprint;

class Users extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $this->getContainer()['db']->schema()->create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('account_id', 128)->unique();
            $table->string('domain', 128)->nullable();
            $table->json('amocrm_token')->nullable();
            $table->string('unisender_token', 512)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $this->getContainer()['db']->schema()->dropIfExists('users');
    }
}
