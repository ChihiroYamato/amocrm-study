<?php

declare(strict_types=1);

use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule;
$capsule->addConnection([
    'driver'    => 'mysql',
    'host'      => getenv('DB_HOST') ?? '',
    'database'  => getenv('DB_DATABASE') ?? '',
    'username'  => getenv('DB_ROOT') ?? '',
    'password'  => getenv('DB_ROOT_PASSWORD') ?? '',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
]);
$capsule->setAsGlobal();
$capsule->bootEloquent();
