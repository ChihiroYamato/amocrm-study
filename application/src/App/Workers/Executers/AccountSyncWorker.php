<?php

declare(strict_types=1);

namespace App\Workers\Executers;

use App\Models\User;
use App\Services\AmoContactsService;
use App\Services\UnisenderContactsService;
use Symfony\Component\Console\Output\OutputInterface;

class AccountSyncWorker extends BeanstalkWorker
{
    public const NAME = 'crm:worker:account_sync';
    public const QUEUE = 'account_sync';

    protected function configure(): void
    {
        $this->setHelp('Command synchonize all contacts of all accounts from AmoCRM to Unisender');
    }

    protected function getModel(): string
    {
        return User::class;
    }

    /**
     * @throws \App\Exceptions\BeanstalkProcessException
     */
    protected function process(string $job, OutputInterface $output): void
    {
        $user = unserialize($job);

        if (! ($user instanceof User)) {
            throw new \App\Exceptions\BeanstalkProcessException('Uncorrect Model');
        }

        $output->writeln(sprintf('Jod model: %s', get_class($user)));

        if ($user->unisender_token === null) {
            return;
        }

        $result = (new UnisenderContactsService($user->unisender_token))->synchronize((new AmoContactsService($user))->getAll());

        $output->writeln(print_r($result, true));
    }
}
