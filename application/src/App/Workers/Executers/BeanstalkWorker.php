<?php

declare(strict_types=1);

namespace App\Workers\Executers;

use App\Models\Beanstalk;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Pheanstalk\Contract\PheanstalkInterface;

abstract class BeanstalkWorker extends Command
{
    public const NAME = '';
    public const QUEUE = '';

    public function __construct()
    {
        parent::__construct(static::NAME);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->getFormatter()->setStyle('note', new OutputFormatterStyle('#FFA500', null, ['bold', 'blink']));

        try {
            $connect = (new Beanstalk($this->getModel(), static::QUEUE))->getConnect();

            $output->writeln(sprintf('<info>Jobs Started:</info> <note>%s</note>', static::QUEUE));

            while (($job = $connect->watchOnly(static::QUEUE)->ignore(PheanstalkInterface::DEFAULT_TUBE)->reserveWithTimeout(0)) !== null) {
                try {
                    $this->process($job->getData(), $output);

                    $output->writeln(sprintf('<note>Job %s - <note><info>done</info>', $job->getId()));

                    $connect->delete($job);
                } catch (\Throwable $exception) {
                    $connect->release($job);
                    $output->writeln(sprintf('<error>%s</error>', $exception->getMessage()));
                }
            }
        } catch (\Throwable $exception) {
            $output->writeln(sprintf('<error>%s</error>', $exception->getMessage()));
            return Command::FAILURE;
        }

        $output->writeln(sprintf('<info>Jobs Done:</info> <note>%s</note>', static::QUEUE));

        return Command::SUCCESS;
    }

    /**
     * Метод возвращает корректное имя класса модели для загрузки в очередь работ
     *
     * @return string
     */
    abstract protected function getModel(): string;

    /**
     * Метод выполняет конкретную работу
     *
     * @param string $job
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * @return void
     */
    abstract protected function process(string $job, OutputInterface $output): void;
}
