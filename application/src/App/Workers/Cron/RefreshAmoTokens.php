<?php

declare(strict_types=1);

namespace App\Workers\Cron;

use App\Models\User;
use App\Services\AmoClientConnect;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;

/**
 * Класс реализация консольной команды по обновлению токенов всех аккаунтов
 */
class RefreshAmoTokens extends Command
{
    public const HOUR_EXPIRES = 24;

    protected static $defaultName = 'crm:cron:refresh';
    protected static $defaultDescription = 'refreshing tokens from AmoCRM';

    protected function configure(): void
    {
        $this->setHelp('This command download all accounts from DB and start process to refresh tokens');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->getFormatter()->setStyle('note', new OutputFormatterStyle('#FFA500', null, ['bold', 'blink']));

        $output->writeln('<note>Start refreshing</note>');

        foreach (User::where('amocrm_token->expires', '<', $this->getUnixTime(self::HOUR_EXPIRES))->get() as $user) {
            AmoClientConnect::refreshToken($user);
            $output->writeln(sprintf('<note>Token of user %s</note> - <info>Refreshed</info>', $user->account_id));
        }

        $output->writeln('<info>Refreshing finished successful</info>');

        return Command::SUCCESS;
    }

    /**
     * Получение метки времени со сдвигом на произвольное количество часов
     *
     * @param int $hour часы для сдвига
     * @return int
     */
    private function getUnixTime(int $hour): int
    {
        return time() + 60 * 60 * $hour;
    }
}
