<?php

declare(strict_types=1);

namespace App;

/**
 * The configuration provider for the App module
 *
 * @see https://docs.laminas.dev/laminas-component-installer/
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     */
    public function __invoke(): array
    {
        return [
            'laminas-cli'      => $this->getCliConfig(),
            'dependencies'     => $this->getDependencies(),
            'templates'        => $this->getTemplates(),
            'basePath'         => $this->getBaseDirectory(),
            'secretFolderPath' => $this->getSecretFolderPath(),
        ];
    }

    public function getCliConfig() : array
    {
        return [
            'commands' => [
                'crm:worker:account_sync' => Workers\Executers\AccountSyncWorker::class,
                'crm:cron:refresh' => Workers\Cron\RefreshAmoTokens::class
            ],
        ];
    }

    /**
     * Returns the container dependencies
     */
    public function getDependencies(): array
    {
        return [
            'invokables' => [
                Handler\PingHandler::class => Handler\PingHandler::class,
            ],
            'factories'  => [
                Handler\HomePageHandler::class => Handler\HomePageHandlerFactory::class,
            ],
        ];
    }

    /**
     * Returns the templates configuration
     */
    public function getTemplates(): array
    {
        return [
            'paths' => [
                'app'    => ['templates/app'],
                'error'  => ['templates/error'],
                'layout' => ['templates/layout'],
            ],
        ];
    }

    /**
     * Return the base directory of app
     */
    public function getBaseDirectory(): string
    {
        return dirname(__DIR__, 2);
    }

    /**
     * Return the secret folder path of app
     */
    public function getSecretFolderPath(): string
    {
        return $this->getBaseDirectory() . '/secret';
    }
}
