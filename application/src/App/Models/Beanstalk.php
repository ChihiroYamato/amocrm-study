<?php

declare(strict_types=1);

namespace App\Models;

use Pheanstalk\Pheanstalk;
use Illuminate\Database\Eloquent\Model;

/**
 * Модель подключения к Beanstalk
 */
final class Beanstalk
{
    private Pheanstalk $connect;

    /**
     * Инициализирует подключение и загружает работы в очередь
     *
     * @param string $modelClass имя класса от базовой модели
     * @param string $queue имя очереди
     * @return void
     */
    public function __construct(string $modelClass, string $queue)
    {
        if (! (class_exists($modelClass) && is_subclass_of($modelClass, Model::class))) {
            throw new \Exception('Uncorrect Model class');
        }

        $this->connect = Pheanstalk::create(getenv('BEANSTALKD_HOST'));
        $this->connect->useTube($queue);

        foreach ($modelClass::all() as $model) {
            $this->connect->put(serialize($model));
        }
    }

    /**
     * Возвращает экземпляр подключения к Beanstalk
     *
     * @return \Pheanstalk\Pheanstalk
     */
    public function getConnect(): Pheanstalk
    {
        return $this->connect;
    }
}
