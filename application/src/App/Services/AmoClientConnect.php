<?php

declare(strict_types=1);

namespace App\Services;

use App\Models\User;
use AmoCRM\Client\AmoCRMApiClient;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Token\AccessTokenInterface;

/**
 * Класс для получения подключения к конкретному аккаунту
 */
final class AmoClientConnect
{
    /**
     * Возвращает экземпляр клиента AmoCRMApiClient с подключением к аккаунту
     *
     * @param \App\Models\User $user
     * @return \AmoCRM\Client\AmoCRMApiClient
     */
    public static function getConnect(User $user): AmoCRMApiClient
    {
        return self::getClient($user)
            ->setAccessToken(new AccessToken(json_decode($user->amocrm_token, true)))
            ->onAccessTokenRefresh(function (AccessTokenInterface $accessToken, string $baseDomain) use ($user) {
                $user->amocrm_token = json_encode([
                    'access_token' => $accessToken->getToken(),
                    'refresh_token' => $accessToken->getRefreshToken(),
                    'expires' => $accessToken->getExpires(),
                    'baseDomain' => $baseDomain,
                ], JSON_FORCE_OBJECT);
                $user->save();
            });
    }

    /**
     * Обновляет токен у указанного аккаунта
     *
     * @param \App\Models\User $user модель аккаунта
     * @return void
     */
    public static function refreshToken(User $user)
    {
        $accessToken = self::getClient($user)->getOAuthClient()->getAccessTokenByRefreshToken(new AccessToken(json_decode($user->amocrm_token, true)));

        $user->amocrm_token = json_encode([
            'access_token' => $accessToken->getToken(),
            'refresh_token' => $accessToken->getRefreshToken(),
            'expires' => $accessToken->getExpires(),
            'baseDomain' => $user->domain,
        ], JSON_FORCE_OBJECT);

        $user->save();
    }

    /**
     * Возвращает базавое подключение к указанному аккаунту
     *
     * @param \App\Models\User $user модель аккаунта
     * @return \AmoCRM\Client\AmoCRMApiClient
     */
    private static function getClient(User $user): AmoCRMApiClient
    {
        return (new AmoCRMApiClient(getenv('AMO_CLIENT_ID'), getenv('AMO_CLIENT_SECRET'), getenv('AMO_REDIRECT_URL')))
            ->setAccountBaseDomain($user->domain);
    }
}
