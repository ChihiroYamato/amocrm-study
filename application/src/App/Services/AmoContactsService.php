<?php

declare(strict_types=1);

namespace App\Services;

use App\Models\User;
use AmoCRM\Client\AmoCRMApiClient;
use AmoCRM\Exceptions\AmoCRMApiNoContentException;

/**
 * Класс обертка по взаимодейсвию с контактами сервиса AmoCRM посредством библиотеки AmoCRM
 */
final class AmoContactsService
{
    /**
     * @var \AmoCRM\Client\AmoCRMApiClient $amoConnect
     */
    private AmoCRMApiClient $amoConnect;

    public function __construct(User $user)
    {
        $this->amoConnect = AmoClientConnect::getConnect($user);
    }

    /**
     * Получение отформатированого списка контаков вида {name:name, email:[]|null}
     * @return array
     */
    public function getAll(): array
    {
        $result = [];

        try {
            $contacts = $this->amoConnect->contacts()->get();
        } catch (AmoCRMApiNoContentException $e) {
            return [];
        }

        foreach ($contacts as $contact) {
            $email = null;

            foreach ($contact->getCustomFieldsValues() as $customField) {
                if (isset($customField->toArray()['field_code']) && $customField->toArray()['field_code'] === 'EMAIL') {
                    $email = array_column($customField->toArray()['values'], 'value');
                    break;
                }
            }

            $result[] = [
                'name' => $contact->getName(),
                'email' => $email,
            ];
        }

        return $result;
    }
}
