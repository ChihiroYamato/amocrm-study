<?php

declare(strict_types=1);

namespace App\Services;

use Unisender\ApiWrapper\UnisenderApi;

/**
 * Класс обертка для взаимодействия с сервисом Unisender посредством библиотеки Unisender
 */
final class UnisenderContactsService
{
    /**
     * @var string Базовый список для хранения контактов AmoCRM в сервисе Unisender
     */
    private const AMOCRM_LIST = 'AmoCRM рассылка';

    /**
     * @var int Максимально допустимый размер POST DATA при запросе в сервис Unisender
     */
    private const POST_LIMIT = 500;

    /**
     * @var \Unisender\ApiWrapper\UnisenderApi $client
     */
    private UnisenderApi $client;

    public function __construct(string $token)
    {
        $this->client = new UnisenderApi($token);
    }

    /**
     * Метод синхронизирует контакты из сервиса AmoCRM в сервис Unisender
     *
     * @param array $contacts
     * @return array
     * @throws \App\Exceptions\UnisenderException
     */
    public function synchronize(array $contacts): array
    {
        $preparedContacts = $this->prepareContacts($contacts, $this->getAmoListId());

        $result = array_map(function (array $list) {
            $response = json_decode($this->client->importContacts($list), true);
            if (! isset($response['result'])) {
                throw new \App\Exceptions\UnisenderException(sprintf('Unisender error. %s', $response['code'] ?? 'unknown error'));
            }
            return $response;
        }, $preparedContacts);

        return $result;
    }

    /**
     * Метод получает актуальный id списка контактов для AmoCRM в сервисе Unisender
     *
     * @return int
     * @throws \App\Exceptions\UnisenderException
     */
    private function getAmoListId(): int
    {
        return $this->createListIfExist(self::AMOCRM_LIST)['id'];
    }

    /**
     * Метод создает список контактов в сервисе Unisender
     *
     * @param string $list имя списка
     * @return array информация по списку
     * @throws \App\Exceptions\UnisenderException
     */
    private function createListIfExist(string $list): array
    {
        $data = json_decode($this->client->getLists(), true);

        if (! isset($data['result'])) {
            throw new \App\Exceptions\UnisenderException(sprintf('Unisender error. %s.', $data['code'] ?? 'unknown error'));
        }

        $key = array_search($list, array_column($data['result'], 'title'));

        if ($key === false) {
            $data = json_decode($this->client->createList(['title' => $list]), true);

            if (! isset($data['result'])) {
                throw new \App\Exceptions\UnisenderException(sprintf('Unisender error. %s.', $data['code'] ?? 'unknown error'));
            }

            return $data['result'];
        }

        return $data['result'][$key];
    }

    /**
     * Метод подготавливает массив контактов для передачи в Unisender
     *
     * @param array $contacts список контактов
     * @param int $id идетинификатор списка в сервисе Unisender
     * @return array подготовленный список для передачи в Unisender
     */
    private function prepareContacts(array $contacts, int $id): array
    {
        $result = [];
        $fields = [
            'Name',
            'email',
            'email_list_ids',
        ];
        $count = 0;
        $iteration = 0;

        $result[$iteration]['field_names'] = $fields;

        foreach ($contacts as $contact) {
            if ($contact['email'] !== null && is_array(($contact['email']))) {
                foreach ($contact['email'] as $email) {
                    $result[$iteration]['data'][] = [$contact['name'], $email, $id];

                    if ($count++ > self::POST_LIMIT) {
                        $result[++$iteration]['field_names'] = $fields;
                        $count = 0;
                    }
                }
            }
        }

        return array_filter($result, fn (array $item) => ! empty($item['data']));
    }
}
