<?php

declare(strict_types=1);

namespace App\Exceptions;

/**
 * Базовые исключения для ошибок выполнения Beanstalk
 */
final class BeanstalkProcessException extends \Exception {}
