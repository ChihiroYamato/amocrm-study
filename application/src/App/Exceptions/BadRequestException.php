<?php

declare(strict_types=1);

namespace App\Exceptions;

/**
 * Базовые исключения для неккоректного запроса
 */
final class BadRequestException extends \Exception {}
