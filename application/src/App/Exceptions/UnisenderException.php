<?php

declare(strict_types=1);

namespace App\Exceptions;

/**
 * Базовые исключения для ошибок Unisender
 */
final class UnisenderException extends \Exception {}
