<?php

declare(strict_types=1);

namespace App\Handler\AmoCRM;

use App\Models\User;
use AmoCRM\Client\AmoCRMApiClient;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Класс обработчик роута по получению oAuth токенов пользователя
 */
class AuthHandler implements RequestHandlerInterface
{
    /**
     * @var array Обязательные поля запроса
     */
    private const REQUEST_PARAMS = ['code', 'client_id', 'referer'];

    /**
     * Обработка роута по получению oAuth токенов пользователя
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        if (array_key_exists('from_widget', $request->getQueryParams())) {
            return $this->widgetAuth($request);
        }

        return $this->baseAuth($request);
    }

    /**
     * Получение пары Access Token и Refresh Token из временного кода и сохранение в БД
     *
     * @param \AmoCRM\Client\AmoCRMApiClient $apiClient экземлпляр клиента AmoCRMApiClient
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    private function setClientToken(AmoCRMApiClient $apiClient, ServerRequestInterface $request): ResponseInterface
    {
        try {
            foreach (self::REQUEST_PARAMS as $param) {
                if (empty($request->getQueryParams()[$param])) {
                    throw new \Exception(sprintf('Failed data. %s is required', $param));
                }
            }

            if ($request->getQueryParams()['client_id'] !== getenv('AMO_CLIENT_ID')) {
                throw new \Exception('Invalid client_id');
            }

            $apiClient->setAccountBaseDomain($request->getQueryParams()['referer']);

            $accessToken = $apiClient->getOAuthClient()->getAccessTokenByCode($request->getQueryParams()['code']);

            $apiClient->setAccessToken($accessToken);

            $user = User::firstOrNew(['account_id' => $apiClient->account()->getCurrent()->getId()]);
            $user->domain = $apiClient->getAccountBaseDomain();
            $user->amocrm_token = json_encode([
                'access_token' => $accessToken->getToken(),
                'refresh_token' => $accessToken->getRefreshToken(),
                'expires' => $accessToken->getExpires(),
                'baseDomain' => $apiClient->getAccountBaseDomain(),
            ], JSON_FORCE_OBJECT);
            $user->save();

            return new JsonResponse(['status' => 'success', 'result' => $user->account_id]);
        } catch (\Exception $e) {
            return new JsonResponse(['status' => 'failed', 'message' => $e->getMessage()], 400);
        }
    }

    /**
     * Получение oAuth из винджета по WebHook
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    private function widgetAuth(ServerRequestInterface $request): ResponseInterface
    {
        $apiClient = new AmoCRMApiClient(getenv('AMO_CLIENT_ID'), getenv('AMO_CLIENT_SECRET'), getenv('AMO_REDIRECT_URL'));

        return $this->setClientToken($apiClient, $request);
    }

    /**
     * Получение oAut по базовому url
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    private function baseAuth(ServerRequestInterface $request): ResponseInterface
    {
        $apiClient = new AmoCRMApiClient(getenv('AMO_CLIENT_ID'), getenv('AMO_CLIENT_SECRET'), getenv('AMO_REDIRECT_URL'));

        if (! isset($request->getQueryParams()['code'])) {
            $state = bin2hex(random_bytes(16));
            $_SESSION['oauth2state'] = $state;

            $authorizationUrl = $apiClient->getOAuthClient()->getAuthorizeUrl([
                'state' => $state,
                'mode' => 'post_message',
            ]);

            header('Location: ' . $authorizationUrl);
            die;
        }

        if (empty($request->getQueryParams()['state']) || empty($_SESSION['oauth2state']) || ($request->getQueryParams()['state'] !== $_SESSION['oauth2state'])) {
            unset($_SESSION['oauth2state']);

            return new JsonResponse(['status' => 'failed', 'message' => 'Invalid state'], 400);
        }

        return $this->setClientToken($apiClient, $request);
    }
}
