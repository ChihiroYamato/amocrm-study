<?php

declare(strict_types=1);

namespace App\Handler\AmoCRM;

use App\Models\User;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Laminas\Diactoros\Response\JsonResponse;

/**
 * Класс обработчик роута по удалению пользователя интеграцией
 */
class DeleteUserHandler implements RequestHandlerInterface
{
    /**
     * Обработка роута по удалению пользователя интеграцией
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        try {
            $query = $request->getQueryParams();

            if (empty($query['client_uuid']) || $query['client_uuid'] !== getenv('AMO_CLIENT_ID')) {
                throw new \App\Exceptions\BadRequestException('Invalid client_uuid');
            }

            User::where('account_id', $query['account_id'] ?? '0')->firstOrFail()->delete();

            return new JsonResponse(['status' => 'success']);
        } catch (\App\Exceptions\BadRequestException $e) {
            return new JsonResponse(['status' => 'failed', 'message' => $e->getMessage()]);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return new JsonResponse(['status' => 'success']);
        }
    }
}
