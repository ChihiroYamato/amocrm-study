<?php

declare(strict_types=1);

namespace App\Handler\Unisender;

use App\Models\User;
use App\Services\AmoContactsService;
use App\Services\UnisenderContactsService;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Класс обработчик роута по сохранению Unisender Token в БД и базовой синхронизации контактов с AMO
 */
class BaseHandler implements RequestHandlerInterface
{
    /**
     * @var array Обязательные поля запроса
     */
    private const REQUEST_PARAMS = ['token', 'account_id'];

    /**
     * Обработка роута по сохранению Unisender Token в БД и базовой синхронизации контактов с AMO
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        try {
            $data = json_decode(file_get_contents('php://input'), true);

            foreach (self::REQUEST_PARAMS as $param) {
                if (empty($data[$param])) {
                    throw new \App\Exceptions\BadRequestException(sprintf('Failed data. %s is required', $param));
                }
            }

            $user = User::where('account_id', $data['account_id'])->firstOr(function () use ($data) {
                throw new \Illuminate\Database\Eloquent\ModelNotFoundException(sprintf('account by id %s does not exist', $data['account_id']));
            });

            $user->unisender_token = $data['token'];
            $user->save();

            $result = (new UnisenderContactsService($user->unisender_token))->synchronize((new AmoContactsService($user))->getAll());

            return new JsonResponse(['status' => 'success', 'result' => $result], 200, ['Access-Control-Allow-Origin' => '*']);
        } catch (\App\Exceptions\BadRequestException | \Illuminate\Database\Eloquent\ModelNotFoundException | \App\Exceptions\UnisenderException $e) {
            return new JsonResponse(['status' => 'failed', 'message' => $e->getMessage()], 400, ['Access-Control-Allow-Origin' => '*']);
        }
    }
}
