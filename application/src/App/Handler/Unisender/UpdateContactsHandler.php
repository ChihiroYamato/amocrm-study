<?php

declare(strict_types=1);

namespace App\Handler\Unisender;

use App\Models\User;
use App\Services\UnisenderContactsService;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Laminas\Diactoros\Response\JsonResponse;

/**
 * Класс обработчик роута по сохранению/обновлению контакта из webhook AmoCRM в Unisender
 */
class UpdateContactsHandler implements RequestHandlerInterface
{
    /**
     * Обработчик роута по сохранению/обновлению контакта из webhook AmoCRM в Unisender
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        try {
            $data = $request->getParsedBody();

            if (empty($data['account']['id'])) {
                throw new \App\Exceptions\BadRequestException('Failed data. {account: id} is required');
            }

            $user = User::where('account_id', $data['account']['id'])->firstOr(function () use ($data) {
                throw new \Illuminate\Database\Eloquent\ModelNotFoundException(sprintf('account by id %s does not exist', $data['account']['id']));
            });

            if ($user->unisender_token === null) {
                throw new \App\Exceptions\UnisenderException('Unisender token does not setup');
            }

            $result = (new UnisenderContactsService($user->unisender_token))->synchronize($this->prepareContacts($data['contacts']['update'] ?? $data['contacts']['add'] ?? []));

            return new JsonResponse(['status' => 'success', 'result' => $result]);
        } catch(\App\Exceptions\BadRequestException | \Illuminate\Database\Eloquent\ModelNotFoundException | \App\Exceptions\UnisenderException $e) {
            return new JsonResponse(['status' => 'failed', 'message' => $e->getMessage()], 400);
        }
    }

    /**
     * Метод подготавливает список контактов из webhook request для отправки в Unisender
     *
     * @param array $contacts базовый список из webhook request
     * @return array
     */
    private function prepareContacts(array $contacts):  array
    {
        $result = [];

        foreach ($contacts as $contact) {
            $email = null;

            foreach ($contact['custom_fields'] ?? [] as $customField) {
                if (isset($customField['code']) && $customField['code'] === 'EMAIL') {
                    $email = array_column($customField['values'], 'value');
                    break;
                }
            }

            $result[] = [
                'name' => $contact['name'] ?? null,
                'email' => $email,
            ];
        }

        return $result;
    }
}
