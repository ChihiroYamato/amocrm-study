<?php

declare(strict_types=1);

/*
    Crontab by php 8.0

    TODO: учет дней недели
    TODO: учет системного времени обработки скрипта при засыпании
*/

define('WEEK_DAYS', ['SUN', 'MON', 'TUES', 'WED', 'THURS', 'FRI', 'SAT', '*']);

function getMinShift(string $base, int $currentTime, $unit): int
{
    if ($base === '*') {
        return 0;
    }

    $minArr = array_map(function (string $item) use ($currentTime, $unit) {
        if (strpos($item, '/') !== false) {
            $item = (int) substr($item, strpos($item, '/') + 1);
            return abs($item) % $unit;
        }

        $result = ((int) $item) - $currentTime;
        return $result > 0 ? $result : $result + $unit;

    }, explode(',', $base));

    return min($minArr);
}

$crontab = explode(PHP_EOL, file_get_contents(__DIR__ . '/crontab'));
$crontab = array_map(fn (string $line) => explode(' ', $line), $crontab);
$crontab = array_filter($crontab, function (array $instruction) {
    if (count($instruction) !== 6 || ! in_array($instruction[4], WEEK_DAYS)) {
        return false;
    }

    foreach (array_slice($instruction, 0, -2) as $line) {
        if (preg_match('/[0-9\,\*\/]+/', $line) === 0) {
            return false;
        }
    }

    return true;
});

while (true) {
    $works = [];
    $currentTime = new DateTime('now', new DateTimeZone('Europe/Moscow'));

    foreach ($crontab as $work) {
        [$min, $hour, $day, $month, $weekDay, $command] = $work;

        $minShif = getMinShift($min, (int) $currentTime->format('i'), 60);
        $hourShift = getMinShift($hour, (int) $currentTime->format('H'), 24);
        $dayShift = getMinShift($day, (int) $currentTime->format('d'), 31);
        $monthShift = getMinShift($month, (int) $currentTime->format('m'), 12);
        $weekDayShift = 0;

        $sleepTime = $minShif * 60 + $hourShift * 60 * 60 + $dayShift * 24 * 60 * 60;

        $works[] = [
            'time' => $sleepTime ?: 60,
            'command' => $command,
        ];

        unset($min, $hour, $day, $month, $weekDay, $command, $minShif, $hourShift, $dayShift, $monthShift, $weekDayShift, $sleepTime, $work);
    }

    $times = array_column($works, 'time');

    $minSleep = min($times);
    $currentWork = $works[array_search($minSleep, $times)]['command'];

    unset($times, $currentTime, $works);

    sleep($minSleep);
    exec($currentWork . ' > /dev/null &');
}
